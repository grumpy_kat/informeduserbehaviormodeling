from gensim.corpora.dictionary import Dictionary
import json
from gensim import models
import os

jsonObj = json.load(open("all_text_sessions.json", "r"))
sessions = []
for s in jsonObj:
    sessions.append(s['actions'])
print(len(sessions))
print(sessions[0])

dictionary = Dictionary(sessions)
print(dictionary)
corpus = [dictionary.doc2bow(text) for text in sessions]

safe_model = "model_8topics"
if os.path.isfile(safe_model):
    lda = models.LdaModel.load(safe_model)
else:
    lda = models.LdaModel(corpus, num_topics=8)
    lda.save(safe_model)

per_topic_sessions = {}
for s in corpus:
    vector = lda[s]
    maxm = 0
    maxt = 0
    for t in vector:
        if t[1] > maxm:
            maxm = t[1]
            maxt = t[0]
    if per_topic_sessions.get(maxt) is None:
        per_topic_sessions[maxt] = 1
    else:
        per_topic_sessions[maxt] += 1
print(per_topic_sessions)
