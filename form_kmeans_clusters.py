import numpy as np
import random
import sys
import json
from tqdm import *
import codecs
import pickle
from sklearn.cluster import KMeans
import os

f = "sc.txt"
syscall_names = []
for l in codecs.open(f, "r").readlines():
    syscall_names.append(l.split("\t")[1].replace("\n", "").replace("\r", ""))
# create dictionaries with actions and their indexing for further transformation to
# one-hot-encoding and back
actions = syscall_names
print('total actions:', len(actions))
action_indices = dict((c, i) for i, c in enumerate(actions))
indices_action = dict((i, c) for i, c in enumerate(actions))

pad_length = 1000 #2500

jsonObj = json.load(open("all_text_sessions.json", "r"))
seqs = []
session_ids = []
for s in jsonObj:
    seqs.append(s['actions'])
    session_ids.append(s['id'])

print('corpus length:', len(seqs))

# cut too long sessions to maximal size of 99 + 1
# throw away sessions less than 3 actions long
# pad the sessions to be equal length all
input_seqs = []
seq_ids = []
i = 0
for seq in seqs:
    if len(seq) > pad_length:
        input_seqs.append(seq[:pad_length])
        seq_ids.append(session_ids[i])
    if len(seq) <= pad_length and len(seq) > 2:
        pad_len = pad_length - len(seq)
        pad_seq = [None] * pad_len
        pad_seq.extend(seq)
        input_seqs.append(pad_seq)
        seq_ids.append(session_ids[i])
    i += 1
print('nb inputs:', len(input_seqs))

print('length of session ids', len(session_ids))
print('length of seq ids', len(seq_ids))

# transform sequences and labels to one-hot-encoding vectors
X = []
for i, inp in enumerate(input_seqs):
    vector_seq = np.zeros((len(inp), len(actions)))
    for t, action in enumerate(inp):
        if action is not None:
            vector_seq[t, action_indices[action]] = 1
    X.append(vector_seq)

X = np.array(X)
X = np.reshape(X, (X.shape[0], X.shape[1]*X.shape[2]))
print(X.shape)
if not os.path.isfile("kmeans8_adfald"):
    kmeans = KMeans(n_clusters=8, random_state=0, verbose=0, copy_x=False, max_iter=200).fit(X)
    pickle.dump(kmeans, open("kmeans8_adfald", "wb"))
else:
    kmeans = pickle.load(open("kmeans8_adfald", "rb"))

labels = kmeans.predict(X)
with open("session_clusters_kmeans_adfald_fixed.txt", "w") as outp:
    for i in range(len(X)):
        outp.write(str(seq_ids[i]) + "\t" + str(labels[i]) + "\n")



