# Informed User and Entity Behavior Modeling System

This repository contains an informed user behavior modeling system as described in the paper "Anomaly-based Intrusion Detection using Informed Behavior Clustering and Modeling" by Adilova, Chen, Natious, Thonnard, and Kamp.