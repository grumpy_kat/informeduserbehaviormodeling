from __future__ import print_function
import keras.backend as K
from keras.models import Sequential
from keras.models import load_model
from keras.layers import Dense, Activation, Dropout
from keras.callbacks import EarlyStopping
from keras.layers import LSTM
from keras.optimizers import RMSprop, SGD, Adam
from keras.utils.data_utils import get_file
import numpy as np
import random
import sys
import json
from tqdm import *
from hyperopt import Trials, STATUS_OK, tpe
from hyperas import optim
from hyperas.distributions import choice, uniform, conditional
from keras.models import load_model
from IPython.display import SVG
from keras.utils.vis_utils import model_to_dot
from keras.utils import plot_model, print_summary
from IPython.display import Image
tqdm.monitor_interval = 0
import matplotlib.pyplot as plt

import os
os.environ['CUDA_VISIBLE_DEVICES'] = "0,1,2,3"

import codecs

def data():
    pad_length = 2500

    jsonObj = json.load(open("all_text_sessions.json", "r"))
    seqs = []
    for s in jsonObj:
        seqs.append(s['actions'])

    print('corpus length:', len(seqs))

    f = "sc.txt"
    syscall_names = []
    for l in codecs.open(f, "r").readlines():
        syscall_names.append(l.split("\t")[1].replace("\n", "").replace("\r", ""))
    # create dictionaries with actions and their indexing for further transformation to
    # one-hot-encoding and back
    actions = syscall_names
    print('total actions:', len(actions))
    action_indices = dict((c, i) for i, c in enumerate(actions))
    indices_action = dict((i, c) for i, c in enumerate(actions))

    # cut too long sessions to maximal size of 99 + 1
    # throw away sessions less than 3 actions long
    # pad the sessions to be equal length all
    input_seqs = []
    next_acts = []
    for seq in seqs:
        if len(seq) > pad_length:
            input_seqs.append(seq[:pad_length - 1])
            next_acts.append(seq[pad_length])
        if len(seq) <= pad_length and len(seq) > 2:
            pad_len = pad_length - len(seq)
            pad_seq = [None] * pad_len
            pad_seq.extend(seq[:-1])
            input_seqs.append(pad_seq)
            next_acts.append(seq[-1])
    print('nb inputs:', len(input_seqs))

    # transform sequences and labels to one-hot-encoding vectors
    X = []
    y = []
    for i, inp in enumerate(input_seqs):
        vector_seq = np.zeros((len(inp), len(actions)))
        for t, action in enumerate(inp):
            if action is not None:
                vector_seq[t, action_indices[action]] = 1
        X.append(vector_seq)
        vector_label = np.zeros((len(actions)))
        vector_label[action_indices[next_acts[i]]] = 1
        y.append(vector_label)

    train_size = int(round(len(X) * 0.7))
    val_size = int(round(len(X) * 0.85))
    x_train = np.asarray(X[:train_size])
    y_train = np.asarray(y[:train_size])
    x_val = np.asarray(X[train_size:val_size])
    y_val = np.asarray(y[train_size:val_size])
    x_test = np.asarray(X[val_size:])
    y_test = np.asarray(y[val_size:])

    return x_train, y_train, x_val, y_val, x_test, y_test


def create_model(x_train, y_train, x_val, y_val, x_test, y_test):
    print('Build model...')
    model = Sequential()
    # have several parameters to choose with meta-parametrization optimization
    model.add(LSTM({{choice([128, 256, 512])}}, input_shape=(None, len(actions))))
    model.add(Dropout({{uniform(0, 1)}}))
    model.add(Dense(len(actions)))
    model.add(Activation('softmax'))
    optimizer = Adam(lr={{uniform(0.001, 0.01)}})

    model.compile(loss='categorical_crossentropy', metrics=['accuracy'], optimizer=optimizer)

    result = model.fit(x_train, y_train,
                       batch_size={{choice([16, 32, 64])}},
                       epochs=10,
                       verbose=2,
                       shuffle=True,
                       validation_data=(x_val, y_val))
    # get the highest validation accuracy of the training epochs
    validation_acc = np.amax(result.history['val_acc'])
    print('Best validation acc of epoch:', validation_acc)
    return {'loss': -validation_acc, 'status': STATUS_OK, 'model': model}


best_run, best_model = optim.minimize(model=create_model,
                                      data=data,
                                      algo=tpe.suggest,
                                      max_evals=5,
                                      trials=Trials())

print("Best performing model chosen hyper-parameters:")
print(best_run)
print(best_model.summary())
